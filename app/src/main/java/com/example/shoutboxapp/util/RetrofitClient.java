package com.example.shoutboxapp.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient
{
    public static Retrofit getClient()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://tgryl.pl/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
