package com.example.shoutboxapp.exceptions;

import java.io.IOException;

public class NoInternetConnectionException extends IOException {}
