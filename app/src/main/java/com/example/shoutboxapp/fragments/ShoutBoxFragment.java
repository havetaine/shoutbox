package com.example.shoutboxapp.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.shoutboxapp.adapters.MessageAdapter;
import com.example.shoutboxapp.R;
import com.example.shoutboxapp.api.ChatGetter;
import com.example.shoutboxapp.api.Message;
import com.example.shoutboxapp.exceptions.NoInternetConnectionException;
import com.example.shoutboxapp.util.CheckInternet;
import com.example.shoutboxapp.util.IdGetter;
import com.example.shoutboxapp.util.RecyclerViewClickListener;
import com.example.shoutboxapp.util.RetrofitClient;
import com.example.shoutboxapp.util.ThrowingOnRefresh;
import com.example.shoutboxapp.util.ThrowingRunnable;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ShoutBoxFragment extends Fragment
{
    private View view;
    private RecyclerView shoutboxRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerViewClickListener listener;
    private IdGetter idGetter;
    private RecyclerView.Adapter<MessageAdapter.MessageViewHolder> adapter;
    private String login;
    private EditText messageField;
    private List<Message> items;
    private CheckInternet check;
    private AppCompatImageButton sendButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_shoutbox, container, false);

        check = new CheckInternet(view.getContext());
        shoutboxRecyclerView = view.findViewById(R.id.recycyle_view);
        shoutboxRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        swipeRefreshLayout = view.findViewById(R.id.refresh_layout);
        messageField = view.findViewById(R.id.MessageField);
        sendButton = view.findViewById(R.id.sendMessageButton);
        shoutboxRecyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(throwingOnRefreshListenerWrapper(this::getMessages));


        login = getArguments().getString("LOGIN");

        listener = (clicked) ->
                showMessage(login, clicked.getLogin(), clicked.getContent(), clicked.getId());

        sendButton.setOnClickListener(v ->
        {
            sendMessage(login, messageField.getText().toString());
            messageField.setText("");
            try
            {
                getMessages();
            }
            catch (NoInternetConnectionException e)
            {
                sendButton.setEnabled(false);
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(throwingRunnableWrapper(this::getMessages), 60000);

        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT)
        {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
            {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
            {
                String swipedElementLogin = items.get(viewHolder.getAdapterPosition()).getLogin();
                if(swipedElementLogin.equals(login))
                {
                    deleteMessage(items.get(viewHolder.getAdapterPosition()).getId());
                    try
                    {
                        getMessages();
                    }
                    catch (NoInternetConnectionException e)
                    {
                        sendButton.setEnabled(false);
                    }
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(shoutboxRecyclerView);

        handler.post(throwingRunnableWrapper(this::getMessages));

        return view;
    }

    private void getMessages() throws NoInternetConnectionException
    {
        if(!check.isNetworkConnected() || check.isInternetAvailable())
        {
            throw new NoInternetConnectionException();
        }

        sendButton.setEnabled(true);

        if (swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }

        Retrofit retrofit = RetrofitClient.getClient();
        ChatGetter chatGetter = retrofit.create(ChatGetter.class);
        Call<List<Message>> call = chatGetter.getData();
        call.enqueue(new Callback<List<Message>>()
        {
            @Override
            public void onResponse(@NonNull Call<List<Message>> call, @NonNull Response<List<Message>> response)
            {
                if (!response.isSuccessful())
                {
                    Toast.makeText(view.getContext().getApplicationContext(), "Couldn't download comments.", Toast.LENGTH_SHORT).show();
                    return;
                }

                List<Message> chatData = response.body();

                adapter = new MessageAdapter(view.getContext().getApplicationContext(), chatData, listener, idGetter);
                shoutboxRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<Message>> call, @NonNull Throwable t)
            {
                Toast.makeText(view.getContext().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                t.printStackTrace();
                call.cancel();
            }
        });

        idGetter = (swiped) ->
                items = swiped;
    }

    private void sendMessage(String login, String content)
    {
        Message message = new Message(content, login);
        Retrofit retrofit = RetrofitClient.getClient();
        ChatGetter send = retrofit.create(ChatGetter.class);
        Call<Message> call = send.send(message);

        call.enqueue(new Callback<Message>()
        {
            @Override
            public void onResponse(@NonNull Call<Message> call, @NonNull Response<Message> response)
            {
                if (!response.isSuccessful())
                {
                    Toast.makeText(view.getContext().getApplicationContext(), "Couldn't send comment.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Message> call, @NonNull Throwable t)
            {
                Toast.makeText(view.getContext().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    private void deleteMessage(String id)
    {
        Retrofit retrofit = RetrofitClient.getClient();
        ChatGetter delete = retrofit.create(ChatGetter.class);
        Call<Message> call = delete.delete(id);

        call.enqueue(new Callback<Message>()
        {
            @Override
            public void onResponse(@NonNull Call<Message> call, @NonNull Response<Message> response)
            {
                if (!response.isSuccessful())
                {
                    Toast.makeText(view.getContext().getApplicationContext(), "The comment can't be deleted.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Message> call, @NonNull Throwable t)
            {
                Toast.makeText(view.getContext().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    public void showMessage(String usersLogin, String sentLogin, String content, String id)
    {
        MessageViewer mv = new MessageViewer();
        Bundle bundle = new Bundle();
        bundle.putString("LOGIN", usersLogin);
        bundle.putString("SENT_LOGIN", sentLogin);
        bundle.putString("MESSAGE", content);
        bundle.putString("ID", id);
        mv.setArguments(bundle);
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, mv);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    private Runnable throwingRunnableWrapper(ThrowingRunnable<NoInternetConnectionException> throwingRunnable)
    {
        return () ->
        {
            try
            {
                throwingRunnable.run();
            }
            catch (NoInternetConnectionException e)
            {
                sendButton.setEnabled(false);
                Toast.makeText(view.getContext().getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private  SwipeRefreshLayout.OnRefreshListener throwingOnRefreshListenerWrapper(ThrowingOnRefresh<NoInternetConnectionException> throwingOnRefresh)
    {
        return () ->
        {
            try
            {
                throwingOnRefresh.onRefresh();
            }
            catch (NoInternetConnectionException e)
            {
                sendButton.setEnabled(false);
                Toast.makeText(view.getContext().getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
            }
        };
    }
}
