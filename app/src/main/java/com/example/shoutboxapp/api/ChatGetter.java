package com.example.shoutboxapp.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ChatGetter
{
    @GET("shoutbox/messages")
    Call<List<Message>> getData();

    @POST("shoutbox/message")
    Call<Message> send(@Body Message message);

    @PUT("shoutbox/message/{id}")
    Call<Message> update(@Path("id") String id, @Body Message message);

    @DELETE("shoutbox/message/{id}")
    Call<Message> delete(@Path("id") String id);
}
