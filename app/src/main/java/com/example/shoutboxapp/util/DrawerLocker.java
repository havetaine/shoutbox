package com.example.shoutboxapp.util;

public interface DrawerLocker
{
    void lockDrawer();
    void unlockDrawer();
}
