package com.example.shoutboxapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shoutboxapp.R;
import com.example.shoutboxapp.api.Message;
import com.example.shoutboxapp.util.IdGetter;
import com.example.shoutboxapp.util.RecyclerViewClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>
{
    private Context context;
    private final List<Message> items;
    private final RecyclerViewClickListener listener;
    private final IdGetter idGetter;
    public MessageAdapter(Context context, List<Message> items, RecyclerViewClickListener listener,
                          IdGetter idGetter)
    {
        this.context = context;
        this.items = items;
        this.listener = listener;
        this.idGetter = idGetter;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position)
    {
        Message currentItem = items.get(position);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat formattedDate = new SimpleDateFormat("dd-MM-yyyy");
        String finalDate = "24-2-2002";

        try
        {
             finalDate = formattedDate.format(formatter.parse(currentItem.getDate()));
        } catch(ParseException e)
        {
            System.out.println("Data się wzięła nie sparsowała.");
        }

        holder.loginAndDate.setText(new String(currentItem.getLogin() + " " + finalDate));
        holder.text.setText(currentItem.getContent());
        holder.layout.setOnClickListener(v ->
                listener.recyclerViewItemClicked(currentItem));
        idGetter.getId(items);
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder
    {
        private TextView loginAndDate;
        private TextView text;
        private LinearLayout layout;

        public MessageViewHolder(@NonNull View itemView)
        {
            super(itemView);
            loginAndDate = itemView.findViewById(R.id.loginAndDateId);
            text = itemView.findViewById(R.id.messageId);
            layout = itemView.findViewById(R.id.message_item_layout);
        }
    }
}
