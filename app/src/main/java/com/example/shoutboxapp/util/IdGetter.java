package com.example.shoutboxapp.util;

import com.example.shoutboxapp.api.Message;

import java.util.List;

public interface IdGetter
{
    void getId(List<Message> message);
}
