# ShoutBox 💬

**ShoutBox** is a simple chat application for Android devices that I made as an end-of-semester project ✔️.
The aplication was created in Android Studio using Java.  

## Application
### Opening window
Login screen where user can enter their username.  

![](screenshots/login_screen.jpg)  
### Chat
The chat itself ✨. User can send their message or delete by swipe gesture. 

![](screenshots/chat_screen.jpg)  
### Editing the message
Here user can edit their message or delete it.  

![](screenshots/edit_screen.jpg)  

## Build
To build the app please use Android Studio.  

## Technology stack
- Java 17
- Android 6.0+
- HTTP client - [Retrofit](https://square.github.io/retrofit/)
- JSON conversion - [Gson](https://github.com/google/gson)
