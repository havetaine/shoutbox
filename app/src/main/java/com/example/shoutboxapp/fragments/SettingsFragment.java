package com.example.shoutboxapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.shoutboxapp.R;
import com.example.shoutboxapp.util.CheckInternet;
import com.example.shoutboxapp.util.DrawerLocker;

public class SettingsFragment extends Fragment
{
    private View view;
    private String login;
    private EditText loginField;
    private DrawerLocker locker;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        loginField = view.findViewById(R.id.loginField);
        Button button = view.findViewById(R.id.sendButton);
        CheckInternet check = new CheckInternet(view.getContext());

        if(!check.isNetworkConnected() || check.isInternetAvailable())
        {
            button.setEnabled(false);
        }

        button.setOnClickListener(this::saveLogin);

        locker.lockDrawer();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try
        {
            locker = (DrawerLocker) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement MyInterface.");
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        locker.unlockDrawer();
    }

    public void saveLogin(View view)
    {
        if((login = getLogin()).isEmpty())
        {
            Toast toast = Toast.makeText(getContext().getApplicationContext(), "Login required.", Toast.LENGTH_SHORT);
            toast.show();

            return;
        }

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LOGIN", login);
        editor.apply();

        changeFragment();
    }

    private String getLogin()
    {
        String tmp = loginField.getText().toString();
        System.out.println("Pobrano login z pola." + tmp);

        if(tmp.isEmpty())
        {
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
            tmp = sharedPreferences.getString("LOGIN", "");

            login = tmp;
            System.out.println("Pobrano login z sharedPreferences." + login);
        }

        return tmp;
    }

    public void changeFragment()
    {
        ShoutBoxFragment sbf = new ShoutBoxFragment();
        Bundle bundle = new Bundle();
        bundle.putString("LOGIN", login);
        sbf.setArguments(bundle);
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, sbf);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
