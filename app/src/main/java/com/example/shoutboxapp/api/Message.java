package com.example.shoutboxapp.api;

import com.google.gson.annotations.SerializedName;

public class Message
{
    @SerializedName("content")
    private String content;
    @SerializedName("login")
    private String login;
    @SerializedName("date")
    private String date;
    @SerializedName("id")
    private String id;

    public Message(String content, String login)
    {
        this.content = content;
        this.login = login;
        this.date = null;
        this.id = null;
    }

    public String getContent() {
        return content;
    }

    public String getLogin() {
        return login;
    }

    public String getId() {
        return id;
    }

    public String getDate()
    {
        return date;
    }

    @Override
    public String toString()
    {
        return "Message{" +
                "content='" + content + '\'' +
                ", login='" + login + '\'' +
                ", date='" + date + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
