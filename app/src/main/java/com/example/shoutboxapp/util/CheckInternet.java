package com.example.shoutboxapp.util;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.InetAddress;

public class CheckInternet
{
    private Context context;

    public CheckInternet(Context context)
    {
        this.context = context;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("tgryl.pl");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }
}

