package com.example.shoutboxapp.util;

import com.example.shoutboxapp.api.Message;

public interface RecyclerViewClickListener
{
    void recyclerViewItemClicked(Message clicked);
}
