package com.example.shoutboxapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.shoutboxapp.R;
import com.example.shoutboxapp.activities.MainActivity;

public class SplashScreen extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final Handler handler = new Handler();
        handler.postDelayed(() ->
        {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }, 3000);
    }
}