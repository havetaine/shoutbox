package com.example.shoutboxapp.util;

public interface ThrowingOnRefresh<E extends Exception>
{
    void onRefresh() throws E;
}
