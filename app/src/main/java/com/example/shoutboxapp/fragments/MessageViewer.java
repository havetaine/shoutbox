package com.example.shoutboxapp.fragments;

import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.shoutboxapp.R;
import com.example.shoutboxapp.api.ChatGetter;
import com.example.shoutboxapp.api.Message;
import com.example.shoutboxapp.util.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MessageViewer extends Fragment
{
    private  View view;
    private String sentLogin;
    private String usersLogin;
    private String content;
    private String id;
    private boolean canBeEdited = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.message_viewer, container, false);

        TextView login = view.findViewById(R.id.loginAndDateView);
        EditText messageView = view.findViewById(R.id.viewedMessage);
        Button cancel = view.findViewById(R.id.cancelButton);
        Button save = view.findViewById(R.id.saveButton);
        sentLogin = getArguments().getString("SENT_LOGIN");
        usersLogin = getArguments().getString("LOGIN");
        content = getArguments().getString("MESSAGE");
        id = getArguments().getString("ID");

        setHasOptionsMenu(true);

        login.setText(sentLogin);
        messageView.setText(content);
        if(!sentLogin.equals(usersLogin))
        {
            canBeEdited = false;
            messageView.setInputType(InputType.TYPE_CLASS_TEXT);
            save.setEnabled(false);
        }

        save.setOnClickListener((v) ->
        {
            String newContent = messageView.getText().toString();
            updateMessage(newContent);
            getChildFragmentManager().popBackStack();
        });

        cancel.setOnClickListener((v) ->
                getChildFragmentManager().popBackStack());



        return view;
    }

    private void updateMessage(String newContent)
    {
        Message message = new Message(newContent, usersLogin);
        Retrofit retrofit = RetrofitClient.getClient();
        ChatGetter update = retrofit.create(ChatGetter.class);
        Call<Message> call = update.update(id, message);

        call.enqueue(new Callback<Message>()
        {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response)
            {
                if(!response.isSuccessful())
                {
                    Toast.makeText(view.getContext().getApplicationContext(), "Nie zaktualizowano zasobu.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t)
            {
                Toast.makeText(view.getContext().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    private void deleteMessage()
    {
        Retrofit retrofit = RetrofitClient.getClient();
        ChatGetter delete = retrofit.create(ChatGetter.class);
        Call<Message> call = delete.delete(id);

        call.enqueue(new Callback<Message>()
        {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response)
            {
                if(!response.isSuccessful())
                {
                    Toast.makeText(view.getContext().getApplicationContext(), "Nie usunięto zasobu.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t)
            {
                Toast.makeText(view.getContext().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        menu.clear();
        inflater.inflate(R.menu.message_viewer_toolbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if(canBeEdited)
        {
            deleteMessage();
        }

        return super.onOptionsItemSelected(item);
    }
}
